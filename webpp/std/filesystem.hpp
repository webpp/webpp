// Created by moisrex on 8/29/23.

#ifndef WEBPP_STD_FILESYSTEM_HPP
#define WEBPP_STD_FILESYSTEM_HPP

#include "./std.hpp"

#include <filesystem>

namespace webpp::stl::filesystem {
    using namespace ::std::filesystem;
}

namespace webpp::fs {
    using namespace ::std::filesystem;
}

#endif // WEBPP_STD_FILESYSTEM_HPP
